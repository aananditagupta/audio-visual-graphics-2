

var fftSize = 1024;
var numBins = fftSize/2;
var sampleRate = 44100;

var fft = new maximJs.maxiFFT();
var samplePlayer = new maximJs.maxiSample();

var startSecs = 0;
var endSecs = 375; // 6mins 15 ... the length of the track
var dur = 375;

var lightCols;
var lightLevels = [0,0,0,0];


//making the first[0] light respond to the kick drum
var lightRange_0 = [0, 10];
var lightTotal_0 = 0;
var lightMean_0 = 0;
//envelope follower for the kick drum sound
var envFollower0 = new maximEx.envFollower(); 


//making the second[1] light respond to the hit hat
var lightRange_1 = [50, 120];
var lightTotal_1 = 0;
var lightMean_1 = 0;
//envelope follower for the kick drum sound
var envFollower2 = new maximEx.envFollower(); 

//making the third[2] light respond to the high hat
var lightRange_2 = [140, 210];
var lightTotal_2 = 0;
var lightMean_2 = 0;
   //envelope follower for the kick drum sound
var envHat = new maximEx.envFollower(); 

//making the fourth[3] light respond to the hit hat
var lightRange_3 = [8, 40];
var lightTotal_3 = 0;
var lightMean_3 = 0;
//envelope follower for the kick drum sound
var envFollower3 = new maximEx.envFollower(); 

var audioContext;
var audioInit;

function setup()
{
    audioContext = new maximJs.maxiAudio();
    audioContext.play = playLoop;
    audioInit = false;

    createCanvas(windowWidth, windowHeight);

    fft.setup(fftSize, fftSize/2, fftSize/4);

    lightCols = [color(255,0,0), color(255,255,0), color(0,255,0), color(0,0,255)];
    
    envFollower0.sampleRate = 60;
    envHat.sampleRate = 60;
    envFollower2.sampleRate = 60;
    envFollower3.sampleRate = 60;
}


function playLoop()
{

    var sig = samplePlayer.play(1/dur, 44100 * startSecs, 44100 * endSecs);

    //process wave
    fft.process(sig);
    this.output = sig;

}

function draw()
{

    background(0);
    
    if(!audioInit)
    {
        push();
        fill(255);
        textAlign(CENTER);
        textSize(32);
        text("Press any key to start ...", width/2, height/2);
        pop();
        return;
    }

    noStroke();
    fill(255);

    text("startPosition: " + floor(startSecs/60) + "mins " + floor(startSecs%60), 20,20);
    
    lightTotal_0 = 0;
    lightTotal_1 = 0;
    lightTotal_2 = 0;
    lightTotal_3 = 0;
    for(var i=0; i<numBins; i++)
    {
        var w = i * width/numBins;
        var bin = fft.getMagnitude(i);
        bin = constrain(bin/50, 0, 1);
        
        /*changing the FFT lines to a different color if they are within the bounds of lightRange_0 */
        if( i > lightRange_0[0] && i <= lightRange_0[1])
        {
            stroke(255, 0, 0);
            /*Inside the conditional statement use lightTotal_0 to sum the values of all the bins within lightRange_0. Remember to set lightTotal_0 to zero before the for loop first.*/
            lightTotal_0 += bin;
        }
        else if(i > lightRange_1[0] && i <= lightRange_1[1])
        {
            //yellow
            stroke(255,255,0);
            lightTotal_1 += bin;
        }
        else if(i > lightRange_2[0] && i <= lightRange_2[1])
        {
            //green
            stroke(0,255,0);
            lightTotal_2 += bin;
        }
        else if(i > lightRange_3[0] && i <= lightRange_3[1])
        {
            //cyan - blue
            stroke(0,255,255);
            lightTotal_3 += bin;
        }
        else
        {
            /*setting the color to gray if that part of the track isn't being isolated */
            stroke(125);
        }
        
        var h = bin * height;
        line(w, height,w,height -h );
    }
    
    //calculate the mean value for the red light
    lightMean_0 = lightTotal_0 / (lightRange_0[1] - lightRange_0[0]);
    //using the .analyse method of the envelope follower to smooth lightMean_0
    lightMean_0 = envFollower0.analyse(lightMean_0, 0.001, 0.5);
    lightLevels[0] = lightMean_0 * 1.5;
    
     //calculating the mean value for the yellow light
    lightMean_1 = lightTotal_1 / (lightRange_1[1] - lightRange_1[0]);
    //using the .analyse method of the envelope follower to smooth lightMean_1
    //lightMean_1 = envFollower2.analyse(lightMean_1, 0.02, 0.5);
    lightLevels[1] = lightMean_1 * 25;
    
    //calculating the mean value for the green light
    lightMean_2 = lightTotal_2 / (lightRange_2[1] - lightRange_2[0]);
    //using the .analyse method of the envelope follower to smooth lightMean_2
    lightMean_2 = envHat.analyse(lightMean_2, 0.01, 0.5);
    lightLevels[2] = lightMean_2 * 25;
    
     //calculating the mean value for the blue light
    lightMean_3 = lightTotal_3 / (lightRange_3[1] - lightRange_3[0]);
    //using the .analyse method of the envelope follower to smooth lightMean_3
    //lightMean_3 = envFollower3.analyse(lightMean_3, 0.001, 0.5);
    lightLevels[3] = lightMean_3 * 20;
    
    for(var i = 0; i < 4; i++)
    {
        translate(width/5, 0);
        drawLight(lightLevels[i], i);
    }
    

}

function drawLight(input, index)
{
    /*creating a spotlight like shape so that it can blink to the corresponding isolated sound */
    noStroke();
    fill(lightCols[index].levels[0], lightCols[index].levels[1], lightCols[index].levels[2],50 * input);
    beginShape();
    vertex(width/64,height * 1/4);
    vertex(-width/64,height * 1/4);
    vertex(-width/16, height * 3/4);
    vertex(width/16, height * 3/4);
    endShape();
    fill(lightCols[index].levels[0], lightCols[index].levels[1], lightCols[index].levels[2],255 * input);
    ellipse(0,height * 3/4,width/8, width/24);
    ellipse(0,height * 1/4,width/32,width/96);
}

function mouseDragged(){

  //this allows you to scrub through the track
  startSecs = map(mouseX, 0 ,width, 0 , endSecs - 10);
  dur = endSecs - startSecs;
  samplePlayer.trigger();
}

function keyPressed(){
    
  if(!audioInit)
  {
    audioContext.init();
    audioContext.loadSample("assets/televisedGreenSmoke.mp3", samplePlayer);
    audioInit = true;
  }
  else if(key == '1')
  { 
    lightLevels[0] = 1;
  }
  else if(key == '2')
  { 
    lightLevels[1] = 1;
  }
  else if(key == '3')
  { 
    lightLevels[2] = 1;
  }
  else if(key == '4')
  { 
    lightLevels[3] = 1;
  }
}

function keyReleased()
{
    

  if(key == '1')
  {
    lightLevels[0] = 0;
  }
  else if(key == '2')
  {
    lightLevels[1] = 0;
  }
  else if(key == '3')
  {
    lightLevels[2] = 0; 
  }
  else if(key == '4')
  {
    lightLevels[3] = 0;
  }

}

/* Light 1

Describe the sounds that you tried to isolate  (use track times)?
YELLOW
This includes the percussive stinger sound that starts from around 1 min and 32 secs and which eventually turns into a bass line sound. 

How did you do this ?
I have set the array values for the FFT bins from 50 -120. If the bins in that array range increase then the FFT lines change their color to yellow. 

Describe any issues that you had.
I tried to use an envelope follower and if I set the attack time to something as low as 0.001 then the light would never blink. 
The light also takes into account other sounds in the beginning of the sample.

*/

/* Light 2

Describe the sounds that you tried to isolate (use track times)?
GREEN
I have tried to isolate the hit hat sound. It starts from 32 seconds and plays till the end. 

How did you do this ?
I have set the array values for the FFT bins from 140 - 210. If the bins in that array range increase then the FFT lines change their color to green. 

Describe any issues that you had.

*/

/* Light 3

Describe the sounds that you tried to isolate (use track times)?
BLUE
This light tries to isolate the synth pad sound that starts from around 50 seconds and goes on till the end. 

How did you do this ?
I have set the array values for the FFT bins from 8 -40. If the bins in that array range increase then the FFT lines change their color to blue. 

Describe any issues that you had.
I tried to use an envelope follower and if I set the attack time to something as low as 0.001 then the light would never blink. 
*/